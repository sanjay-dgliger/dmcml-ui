import { Button, Card, CardHeader, CardBody, FormGroup, Form, Input, InputGroupAddon, 
         InputGroupText, InputGroup, Row, Col } from "reactstrap";
import { useState } from 'react';
import {  toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';

const Login = () => {
  const [inputs, setInputs] = useState({  email:"", password:"" });
  const [forgot, setForgot] = useState(false);
  const [emailValidation,setEmailValidation] = useState(false); 
  const [passwordError, setPasswordError] = useState({
    name: false,
    email: "",
});
  const history = useHistory();  

const handleChange = (event) => {
    const {name, value} = event.target;
    setInputs((...prev) => {
        return{...prev, [name]:value}
    });
   
    if((/[!@#$%*]/).test(value) ){
      // setEmailValidation(false)
      setPasswordError({name:false})
    }
    else{ 
      // setEmailValidation(true) 
      setPasswordError({name:true})
    }

    //  if(inputs.email){
    //      let regEmail = /^[a-zA-Z0-9]+@(?:[a-zA-Z0-9]+\.)+[A-Za-z]+$/;
    //     if(!regEmail.test(inputs.email)){
    //       setEmailError(true); }
    //     else{
    //       setEmailError(false);}

    // }

};
const handleSubmit = (event) => {
    event.preventDefault();
    if(inputs.email === 'DGLiger@dgliger.com' && inputs.password === 'DGLiger@123'){
        toast.success("Login successfully!");
        history.push('/admin/index');
    }
    else{
        toast.error("wrong credentials");       
    }       
}

  return (
    <>
      <Col lg="5" md="7">
        <Card className="bg-secondary shadow border-0">
        { forgot === false ?
         <CardHeader className="bg-transparent pb-3">
            <div className="text-muted text-center mt-2 mb-3">
              <small>Sign in with </small>
            </div>
            <div className="btn-wrapper text-center">
              
            <Button
                className="btn-neutral btn-icon"
                color="default"
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                <span className="btn-inner--icon">
                  <img
                    alt="..."
                    src={
                      require("../../assets/img/icons/common/facebook.svg")
                        .default
                    }
                  />
                </span>
                {/* <span className="btn-inner--text">Facebook</span> */}
              </Button>
              <Button
                className="btn-neutral btn-icon"
                color="default"
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                <span className="btn-inner--icon">
                  <img
                    alt="..."
                    src={
                      require("../../assets/img/icons/common/microsoft.svg")
                        .default
                    } />
                  </span>
                {/* <span className="btn-inner--text">microsoft</span> */}
              </Button>
              
              
              <Button
                className="btn-neutral btn-icon"
                color="default"
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                <span className="btn-inner--icon">
                  <img
                    alt="..."
                    src={
                      require("../../assets/img/icons/common/github.svg")
                        .default
                    }
                  />
                </span>
                {/* <span className="btn-inner--text">Github</span> */}
              </Button>
              <Button
                className="btn-neutral btn-icon"
                color="default"
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                <span className="btn-inner--icon">
                  <img
                    alt="..."
                    src={
                      require("../../assets/img/icons/common/google.svg")
                        .default
                    }
                  />
                </span>
                {/* <span className="btn-inner--text">Google</span> */}
              </Button>
            </div>
          </CardHeader>
        :<></>}  
          { forgot === false ?
            <CardBody className="px-lg-5 py-lg-3">
            <div className="text-center text-muted mb-4">
              <small>Or sign in with credentials</small>
            </div>
            <Form role="form"  onSubmit={handleSubmit}>
              <FormGroup className="mb-3">
                <InputGroup className="input-group-alternative">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-email-83" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    name="email" 
                    placeholder="Email"
                    type="email"
                    autoComplete="new-email"
                    onChange={handleChange}
                  />
                </InputGroup>
              </FormGroup>
              <FormGroup>
                <InputGroup className="input-group-alternative">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-lock-circle-open" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                   name="password" 
                    placeholder="Password"
                    type="text"
                    autoComplete="new-password"
                    onChange={handleChange}
                  />
                 
                </InputGroup>
                {passwordError.name == true ? <span className="err-msg">Length greater than or equal to 8 characters</span> :<span></span>}
              </FormGroup>
              <div className="custom-control custom-control-alternative custom-checkbox">
                <input
                  className="custom-control-input"
                  id=" customCheckLogin"
                  type="checkbox"
                />
                <label
                  className="custom-control-label"
                  htmlFor=" customCheckLogin"
                >
                  <span className="text-muted">Remember me</span>
                </label>
              </div>
              <div className="text-center">
                <Button className="my-4" color="primary" type='submit'>
                  Sign in
                </Button>
              </div>
            </Form>
          </CardBody>
             
             :<>
               <CardBody className="px-lg-5 py-lg-3">
            <div className="text-center  mb-4">
              <small className="forgot">Forgot Password</small>
              <p>Enter your registered email below to receive password reset instruction </p>
            </div>
            <Form role="form"  onSubmit={handleSubmit}>
              <FormGroup className="mb-3">
                <InputGroup className="input-group-alternative">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="ni ni-email-83" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    name="email" 
                    placeholder="Email"
                    type="email"
                    autoComplete="new-email"
                    onChange={handleChange}
                  />
                </InputGroup>
              </FormGroup>
              <div className="text-center">
                <Button className="my-4" color="primary" type='submit'>
                  Send
                </Button>
              </div>
            </Form>
          </CardBody></> } 
          
        </Card>
        <Row className="mt-3">
          <Col xs="6">
            <a className="text-light" href="#">
              { forgot === false ?
              <small  onClick={ ()=>setForgot(true)}>Forgot password?</small> : <small onClick={()=>setForgot(false)} >Back To Login</small>}
            </a>
          </Col>
          {/* <Col className="text-right" xs="6">
            <a
              className="text-light"
              href="#pablo"
              onClick={(e) => e.preventDefault()}
            >
              <small>Create new account</small>
            </a>
          </Col> */}
        </Row>
      </Col>
    </>
  );
};

export default Login;
