import Cron from 'react-cron-generator'
import 'react-cron-generator/dist/cron-builder.css'
import React,{useState} from 'react'
import {Container } from "reactstrap";
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

export default function Scheduler() {
    const [lgShow, setLgShow] = useState(false);

var repeat =[{value:'',value:'Hourly'},{value:'Daily'},{value:'Weekly'},{value:'Monthly'},{value:'Yearly'}]
var Week =[{value:'MON'},{value:'TUE'},{value:'WED'},{value:'THU'},{value:'FRI'},{value:'SAT'},{value:'SUN'}]
var Yearly =[{value:'January'},{value:'February'},{value:'March'},{value:'April'},{value:'May'},{value:'June'},{value:'July'}
            ,{value:'August'},{value:'September'},{value:'October'},{value:'November'},{value:'December'},{value:'July'}]

            // document.getElementsByClassName('multi-date').datepicker({
            //     multidate: true,
            //     format: 'dd-mm-yyyy'
            //   });
  //           var element = document.getElementsByClassName('multi-date');
  //  element.datepicker({
  //       multidate: true,
  //       format: 'dd-mm-yyyy'
  //     });
            
  const [repeatState, setRepeat] = useState('');
  const submit=(e)=>{  console.log('target',document.getElementsByClassName('multi-date')); 
}
  const handleInputChange = (e) =>{
  setRepeat(e.target.value);
  }


  return (
    <>
    <div className='Scheduler mask bg-gradient-default opacity-8'>
    <Container className="py-6 half-bottom-content" fluid>
     {/* <h1 className='text-white'>Cron Job here!!!...</h1> */}
     {/* <Cron
        onChange={(e)=> {setState(e);}}
        value={state}
        showResultText={true}
        showResultCron={true}
        />     
         <button onClick={submit} className="btn btn-primary">Output data</button> */}
        <button onClick={submit} className="btn btn-primary">Output data</button>    

            <Button onClick={() => setLgShow(true)}>Create Schedule</Button>
      <Modal
        size="lg"
        show={lgShow}
        onHide={() => setLgShow(false)}
        aria-labelledby="example-modal-sizes-title-lg"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-modal-sizes-title-lg">
            Create Scheduler
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <form action="" className='Scheduler-form'>

          <div className="row">
              <label htmlFor="" className='col-12'>Title</label>
              <input type="text" className='col-12' />
          </div>

          
          <div className="row date-section">
            <div className="col-6 start-date">
              <div className="row">
              <label htmlFor="" className='col-12'>Start Date</label>
              <input type="datetime-local" className='col-12' />
            </div>
          </div>

            <div className="col-6 end-date">
              <div className="row">
              <label htmlFor="" className='col-12'>End Date</label>
              <input type="datetime-local" className='col-12'/>
            </div>
          </div>
        </div>
          
        <div className="row">
              <label htmlFor="" className='col-12'>Description</label>
              <textarea name="" id="" cols="30" rows="3" className='col-12'></textarea>
          </div>
          <div className="row">
            <label htmlFor="">Repeat</label> 
            <label class="switch">
            <input type="checkbox" />
            <span class="slider round"></span>
            </label>
          </div>

          <div className="row">
            <select onClick={handleInputChange}>
            {repeat.map((option) => (
              <option value={option.value} >{option.value}</option>
            ))}
          </select>
          </div>
      
          {repeatState =='Hourly' || repeatState =='Daily' ?
          <div className="row">
            <label htmlFor="" className='col-12'>Repeat Every ({repeatState})</label> 
            <input type="number"  className='col-1 p-0' />
          </div>
          :<></>}

          {repeatState =='Weekly' ?
          <>
          <div className="row">
            <label htmlFor="" className='col-12'>Repeat Every ({repeatState})</label> 
            <input type="number"  className='col-1 p-0' />
          </div>

          <div className="row">
          { Week.map((option) => (
          <div class="rating-form">
            <input className='weekInput' type="checkbox" name="rating" value={option.value} id={option.value} />
            <label className='weekLabel' htmlFor={option.value}>{option.value}</label>
     </div>
          ))}
    
</div>

          </>
          :<></>}




{repeatState =='Monthly' ?
          <>
          <div className="row">
            <label htmlFor="" className='col-12'>Repeat Every ({repeatState})</label> 
            <input type="number"  className='col-1 p-0' />
          </div>

          <div className="row">
            <label htmlFor="" className='col-12'>Repeat On ({repeatState})</label> 
            <input type="number"  className='col-1 p-0' />
          </div>

          </>
          :<></>}




{repeatState =='Yearly' ?
          <>
          <div className="row">
            <label htmlFor="" className='col-12'>Repeat Every ({repeatState})</label> 
            <input type="number"  className='col-1 p-0' />
          </div>

          {/* <div className="row">
            <label htmlFor="" className='col-12'>Repeat On ({repeatState})</label> 
            <select >
            {Yearly.map((option) => (
              <option value={option.value} >{option.value}</option>
            ))}
          </select> 
            <input type="number"  className='col-1 p-0' />
          </div> */}


<div className="row">
            <label htmlFor="" className='col-12'>Repeat On ({repeatState})</label> 
            <input type="text" className="form-control multi-date" placeholder="Pick the multiple dates" />
            </div>
          </>
          :<></>}



        </form>

        </Modal.Body>
      </Modal>


        



      </Container>
    </div>
   </>
  )
}

